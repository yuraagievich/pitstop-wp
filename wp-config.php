<?php
/**
 * Основные параметры WordPress.
 *
 * Скрипт для создания wp-config.php использует этот файл в процессе
 * установки. Необязательно использовать веб-интерфейс, можно
 * скопировать файл в "wp-config.php" и заполнить значения вручную.
 *
 * Этот файл содержит следующие параметры:
 *
 * * Настройки MySQL
 * * Секретные ключи
 * * Префикс таблиц базы данных
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define( 'DB_NAME', 'test_pitstop' );

/** Имя пользователя MySQL */
define( 'DB_USER', 'root' );

/** Пароль к базе данных MySQL */
define( 'DB_PASSWORD', 'NtkyC7R_207.1' );

/** Имя сервера MySQL */
define( 'DB_HOST', 'localhost' );

/** Кодировка базы данных для создания таблиц. */
define( 'DB_CHARSET', 'utf8' );

/** Схема сопоставления. Не меняйте, если не уверены. */
define( 'DB_COLLATE', '' );

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется авторизоваться снова.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'a!MI}YNhLk9vb7rg(zu?.v=I5wnKI_pD+j@wAg(OsSsRin)NL*+A#r0Z7KB0Df_D' );
define( 'SECURE_AUTH_KEY',  '^H6 Ltj1[()Ej4rLPK(QR}2S5l:tMXt3j=POsm+U_U#Q)pvJFB>p)OQ3ARuPjy%S' );
define( 'LOGGED_IN_KEY',    ')%-%HA& 0aVMN]aSbLDC#]a+5D4.ftEefJ_{A[=gJ <Vx@i4TrM(C%EdNzQOteAk' );
define( 'NONCE_KEY',        'V75{A1jl]Rsz-MsFrFQJDEg#Yd=t@G1-%Jbtgj$*p#&1{~[n;5,vMEgamz>y`p-:' );
define( 'AUTH_SALT',        '{kokWjgK> fsOan4NRo:]1aD$)A#ksC[IzSgdzJ]V,$<Sh :X+U||t;&xfd0BI?!' );
define( 'SECURE_AUTH_SALT', '?d=w{%PbgK;uilcI}jfc}!hrJ;B]W}}.Xk1?cgUh$;S|g/c.G&0Gu6L(^=`(OdBN' );
define( 'LOGGED_IN_SALT',   'TqMp-C)uep)@_xXep5c)fyCWMBWME%204m (#B*0~8,@}W )K8yQU*A-|}uKp^s^' );
define( 'NONCE_SALT',       'F?%NUv3WTI8Ufdk}#E0-[RJYcq)cpzQ e2*Ht_sqyCmXX$j10W=BgL&H+e#+YCU8' );

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько сайтов в одну базу данных, если использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Разработчикам плагинов и тем настоятельно рекомендуется использовать WP_DEBUG
 * в своём рабочем окружении.
 *
 * Информацию о других отладочных константах можно найти в Кодексе.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', false );

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Инициализирует переменные WordPress и подключает файлы. */
require_once( ABSPATH . 'wp-settings.php' );
