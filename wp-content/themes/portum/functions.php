<?php
/**
 * Portum functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package Portum
 * @since   1.0
 */

/**
 * Load Autoloader
 */
require_once 'inc/class-portum-autoloader.php';
/**
 * Instantiate it
 */
$portum = Portum::get_instance();

register_sidebar( array(
	'name' => __( 'Телефон в шапке','' ),
        'id' => 'top-area',
        'description' => __( 'Шапка','' ),
        'before_widget' =>'',
        'after_widget' =>'',
        'before_title' => '<h3>',
        'after_title' => '</h3>',
    ) );